const Sequelize = require('sequelize');
const db = {};
const sequelize = new Sequelize('laravue', 'root', '', {
    host:'localhost',
    port:3307,
    dialect: 'mysql'
  })

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;